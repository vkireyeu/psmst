# Phase-space Minimum Spanning Tree (psMST)
The main goal was to make an open source library which may be applied to 
any model in the market and be easily included in the experimental 
frameworks. It uses CMake as build system, make is also supported.

psMST implements the MST procedure (with the optional extension to the momentum space) and the coalescence algorithm (only for deuterons).

# {- psMST is now available for experimental collaborations upon request. -}



## How to cite
**"Cluster dynamics studied with the phase-space minimum spanning tree approach"**,  
*V. Kireyeu*,  
[Phys. Rev. C 103, 054905 (2021)](https://journals.aps.org/prc/abstract/10.1103/PhysRevC.103.054905), [arXiv:2103.10542](https://arxiv.org/abs/2103.10542)

**"Deuteron Production in Ultra-Relativistic Heavy-Ion Collisions: A Comparison of the
Coalescence and the Minimum Spanning Tree Procedure"**,  
*V. Kireyeu*, J. Steinheimer, J. Aichelin, M. Bleicher and E. Bratkovskaya  
[Phys. Rev. C 105, 044909 (2022)](https://journals.aps.org/prc/abstract/10.1103/PhysRevC.105.044909), [arXiv:2201.13374](https://arxiv.org/abs/2201.13374)


## Other algorithms 
MST and SACA algorithms used in the [PHQMD](https://phqmd.gitlab.io/) 
model: [Phys. Rev. C 101, 044905](https://journals.aps.org/prc/abstract/10.1103/PhysRevC.101.044905), 
[arxiv:1907.03860](https://arxiv.org/abs/1907.03860)  
Fragment Recognition In General Application (FRIGA): 
[Phys. Rev. C 100, 034904](https://journals.aps.org/prc/abstract/10.1103/PhysRevC.100.034904), 
[arxiv:1906.06162](https://arxiv.org/abs/1906.06162)


## Authors

* **Viktar Kireyeu** - *Initial work*  

# Acknowledgments
Very many thanks to [Elena Bratkovskaya](http://th.physik.uni-frankfurt.de/~brat/index.html) 
and [Joerg Aichelin](https://inspirehep.net/authors/1018831) for the thetoretical support.
I also acknowledge the support by [Jan Steinheimer](https://www.fias.science/de/fellows/detail/steinheimer-froschauer-jan/) and [Marcus Bleicher](https://itp.uni-frankfurt.de/~bleicher/index.html).
